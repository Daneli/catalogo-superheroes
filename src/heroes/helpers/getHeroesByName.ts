import { heroes } from "../data/heroes"


const getHeroesByName = (name: string) => {
    name = name.toLocaleLowerCase().trim()
    if (name.length === 0) return []

    return heroes.filter(
        hero => hero.superhero.toLocaleLowerCase().includes(name)
    )
}

export default getHeroesByName