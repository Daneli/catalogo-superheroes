import { useMemo } from "react";
import { getHeroesByPublisher } from "../helpers/getHeroesByPublisher";
import HeroCards from "./HeroCards";

interface Props {
  publisher: string;
}

const HeroList = ({ publisher }: Props) => {
  const heroes = useMemo(() => getHeroesByPublisher(publisher), [publisher]);

  return (
    <div className="row rows-cols-1 row-cols-md-3 g-3">
      <ul>
        {heroes.map((hero) => (
          <HeroCards firts_appearance={""} key={hero.id} {...hero} />
        ))}
      </ul>
    </div>
  );
};

export default HeroList;
