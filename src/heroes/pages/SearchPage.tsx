import { ChangeEvent } from "react";
import { useForm } from "../../hooks/useForm";
import { useLocation, useNavigate } from "react-router-dom";
import queryString from "query-string";
//import getHeroesByName from "../helpers/getHeroesByName";
import HeroCards from "../components/HeroCards";

const SearchPage = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const { q } = queryString.parse(location.search);
  const { searchText, onInputChange } = useForm({});

  //const heroes = getHeroesByName(q);

  const onHandleSubmitSearch = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();

    if (searchText.trim().length <= 1) return;

    navigate(`?q=${searchText}`);
  };

  return (
    <>
      <h1>Search</h1>
      <hr />

      <div className="row">
        <div className="col-5">
          <h4>Searching</h4>
          <hr />

          <form action="" onSubmit={() => onHandleSubmitSearch}>
            <label htmlFor="search"></label>
            <input
              id="search"
              type="text"
              placeholder="Search a Hero"
              className="form-control"
              name="searchText"
              autoComplete="off"
              value={searchText}
              onChange={() => onInputChange}
            />

            <button title="search" className="btn btn-outline-primary mt-1">
              Search
            </button>
          </form>
        </div>

        <div className="col-7">
          <h4>Results</h4>
          <hr />

          <div className="alert alert-primary">Search a Hero</div>
          <div className="alert alert-danger">
            No hero <b>{q}</b>
          </div>

          <HeroCards
            id={""}
            superhero={""}
            alter_ego={""}
            firts_appearance={""}
            characters={""}
          />

          {/* {heroes.map((hero) => (
            <HeroCards
              firts_appearance={""} key={hero.id}
              {...hero}            />
          ))} */}
        </div>
      </div>
    </>
  );
};

export default SearchPage;
