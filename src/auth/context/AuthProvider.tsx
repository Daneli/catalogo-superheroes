import { useReducer } from "react";
import { AuthContext } from "./AuthContext";
import { AuthReducer } from "./AuthReducer";
import { types } from "../types/types";

interface Props {
  children: string;
}

const initialState = {
  logged: false,
};

const AuthProvider = ({ children }: Props) => {
  const [state, dispatch] = useReducer(AuthReducer, initialState);

  const login1 = (name = "") => {
    const action = {
      type: types.login,
      payload: name,
    };
    dispatch(action);
  };

  return (
    <AuthContext.Provider value={{ state, login1 }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
