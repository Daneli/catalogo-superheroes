import { createContext } from "react";
import { types } from "../types/types";

export const AuthContext = createContext<typeof types | object>(types);
