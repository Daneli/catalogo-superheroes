//import { useContext } from "react";
import { useNavigate } from "react-router-dom";
//import { AuthContext } from "../context/AuthContext";

const LoginPage = () => {
  //const {} = useContext(AuthContext)
  const navigate = useNavigate();

  const onLogin = () => {
    // login('')

    navigate("/", {
      replace: true,
    });
  };

  return (
    <div className="container mt-5">
      <h1>LoginPage</h1>
      <hr />

      <button title="login" className="btn btn-primary" onClick={onLogin}>
        Login
      </button>
    </div>
  );
};

export default LoginPage;
