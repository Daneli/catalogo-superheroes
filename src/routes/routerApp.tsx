import { Route, Routes } from "react-router-dom";

import LoginPage from "../auth/pages/LoginPage";
import { HeroesRoute } from "../heroes/routes/HeroesRoute";

const RouterApp = () => {
  return (
    <>
      <Routes>
        <Route path="/login" element={<LoginPage />} />

        <Route path="/*" element={<HeroesRoute />} />
      </Routes>
    </>
  );
};

export default RouterApp;
