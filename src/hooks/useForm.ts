import { useState } from 'react';

export const useForm = ( initialForm = {} ) => {
  
    const [ formState, setFormState ] = useState( initialForm );

    const onInputChange = (target: { name: string; value: string; }) => {
        const { name, value } = target;
        setFormState({
            ...formState,
            [ name ]: value
        });
    }

    const onResetForm = () => {
        setFormState( initialForm );
    }
    
    const searchText = ''

    return {
        ...formState,
        formState,
        onInputChange,
        onResetForm,
        searchText
    }
}