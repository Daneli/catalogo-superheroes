// import AuthProvider from "./auth/context/AuthProvider";
import RouterApp from "./routes/RouterApp";

const HeroesApp = () => {
  return (
    // <AuthProvider>
    <RouterApp />
    // </AuthProvider>
  );
};

export default HeroesApp;
