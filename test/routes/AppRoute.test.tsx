import React from "react";
import { render, screen } from "@testing-library/react";
import { AuthContext } from "../../src/auth/context/AuthContext";
import { MemoryRouter } from "react-router-dom";
import RouterApp from "../../src/routes/RouterApp";

describe("Testing AppRoute", () => {
  test("Must show login if not exists", () => {
    const contextValue = {
      logged: false,
    };

    render(
      <MemoryRouter initialEntries={["/marvel"]}>
        <AuthContext.Provider value={contextValue}>
          <RouterApp />
        </AuthContext.Provider>
      </MemoryRouter>
    );

    expect(screen.getAllByText("Login").length).toBe(2);
  });

  test("Must show marvel component ", () => {
    const contextValue = {
      logged: true,
      user: {
        id: "ABC",
        name: "Ejemplo",
      },
    };

    render(
      <MemoryRouter initialEntries={["/login"]}>
        <AuthContext.Provider value={contextValue}>
          <RouterApp />
        </AuthContext.Provider>
      </MemoryRouter>
    );

    expect(screen.getAllByAltText("Marvel").length).toBeGreaterThanOrEqual(1);
  });
});
