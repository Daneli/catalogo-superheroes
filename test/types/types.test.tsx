import { types } from "../../src/auth/types/types";

describe("Testing types", () => {
  test("Must return this types", () => {
    expect(types).toEqual({
      login: "[Auth] Login",
      logout: "[Auth] Login",
    });
  });
});
