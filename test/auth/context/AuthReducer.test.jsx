import AuthReducer from "../../../src/auth/context/AuthReducer"
import IAction from "../../../src/auth/interface/IAction"

describe("Testing AuthReducer", () => {
    test('Must return default state', () => {
        const state = AuthReducer({ logged: false }, {})
        expect(state).toBe(state)
    })

    test('Must call authentication login', () => {
        const action = {
            types: types.login,
            payload: 'Ejemplo'
        }


        const state = AuthReducer({ logged: false }, String);

        expect(state).toEqual({
            logged: true,
            user: action.payload
        })
    })

    test('Must delete user name', () => {
        const state = {
            logged: true,
            user: { id: '123', name: 'Juan' }
        }

        const action = {
            type: types.logout,
        }

        const newState = AuthReducer(state, action)
        expect(newState).toEqual({ logged: false })
    })


})