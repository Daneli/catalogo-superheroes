import { fireEvent, render, screen } from '@testing-library/react'
import AuthContext from '../../src/auth/context/AuthContext'
import { MemoryRouter } from 'react-router-dom'
import Navbar from '../../src/ui/components/Navbar'

const mockedUseNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: () => mockedUseNavigate
}))

describe('Testing Navbar', () => {

    const contextValue = {
        logged: true,
        use: {
            name: 'Juan Carlos'
        },
        logout: jest.fn()
    }

    beforeEach(() => jest.clearAllMocks())

    test('Mus show name of the user', () => {
        render(
            <AuthContext.Provider value={contextValue}>
                <MemoryRouter>
                    <Navbar />
                </MemoryRouter>
            </AuthContext.Provider>
        )

        expect(screen.getByText('Ejemplo')).toBeTruthy()
    })

    test('Must call logout and navigate when click in button', () => {
        render(
            <AuthContext.Provider value={contextValue}>
                <MemoryRouter>
                    <Navbar />
                </MemoryRouter>
            </AuthContext.Provider>
        )

        const logout = screen.getByRole('button')
        fireEvent.click(logout);

        expect(contextValue.logout).toHaveBeenCalled()
        expect(mockedUseNavigate).toHaveBeenNthCalledWith("/login", { "replace": true })
    })



})
